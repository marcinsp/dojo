import sys

class Multiplication(object):
    def __init__(self):
        self.right_column = []
        self.left_column = []
        self.strike_out_column = []

    def create_left_column(self, x):
        left_column = [x]
        a = x
        while a > 1:
            a = int(a/2)
            left_column.append(a)

        return left_column

    def create_right_column(self, left_column, y):
        right_column = [y]
        a = y
        for _ in range(len(left_column)-1):
            a = a * 2
            right_column.append(a)
        return right_column

    def strike_out_rows(self, left_column, right_column):
        strike_out_column = []
        for i, value in enumerate(left_column):
            if value % 2 == 0:
                strike_out_column.append(0)
            else:
                strike_out_column.append(right_column[i])
        return strike_out_column

    def get_end_result(self, x, y):
        self.left_column = self.create_left_column(x)
        self.right_column = self.create_right_column(self.left_column, y)
        self.strike_out_column = self.strike_out_rows(self.left_column, self.right_column)
        result = sum(self.strike_out_column)
        return result

    def print_out_result(self, x, y):
        result = self.get_end_result(x, y)
        print_out = ''
        for left_value, right_value in zip(self.left_column, self.strike_out_column):
            print_out += '{left_value}\t{right_value}\n'.format(left_value=left_value, right_value=right_value or '--')
        print_out += '\t===\n'
        print_out += '\t{result}\n'.format(result=result)
        print(print_out)


def main():
    m = Multiplication()
    m.print_out_result(int(sys.argv[1]), int(sys.argv[2]))


if __name__ == '__main__':
    main()
