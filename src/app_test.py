import pytest
from app import Multiplication


@pytest.fixture
def multiplication():
    return Multiplication()


def test_create_left_column(multiplication):
    expected_result = [17, 8, 4, 2, 1]
    assert multiplication.create_left_column(17) == expected_result


def test_create_right_column(multiplication):
    expected_result = [34, 68, 136, 272, 544]
    assert multiplication.create_right_column([17, 8, 4, 2, 1], 34) == expected_result


def test_strike_out_rows(multiplication):
    expected_result = [34, 0, 0, 0, 544]
    assert multiplication.strike_out_rows([17, 8, 4, 2, 1], [34, 68, 136, 272, 544]) == expected_result


def test_get_end_result(multiplication):
    expected_result = 578
    assert multiplication.get_end_result(17, 34) == expected_result
